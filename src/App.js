import React, { Component } from 'react'
import './App.css'
 
// Va permettre de générer nos bouttons
const LETTERS=[
                'A','B','C','D','E','F',
                'G','H','I','J','K','L',
                'M','N','O','P','Q','R',
                'S','T','U','V','W','X',
                'Y','Z'
              ]

// Jeu de test pour notre pendu
const MOTS =[
              'ANGLE','ARMOIRE','BANC','BUREAU',
              'CABINET','CARREAU','CHAISE','CLASSE', 
              'CLEF','COIN','COULOIR','DOSSIER','EAU',
              'ECOLE','ENTRER','ESCALIER','ETAGERE',
              'EXTERIEUR','FENETRE','INTERIEUR','LAVABO',
              'LIT','MARCHE','MATELAS','MATERNELLE','MEUBLE'
            ]

let MOT
// Définition notre objet Letter

const Letter = ({ letter, index, onClick }) => (
  <div className="letter" onClick={() => onClick(index)}>
    <button className="small button">{letter}</button>
  </div>
)

class App extends Component {

  state = {
    letter: '',
    letters: LETTERS,
    usedLetters : [],
    mot_a_deviner : this.generateHideWord(),
    tryCount: 0,
    errorCount: 0,
    won: false,
    warn : '',
    score : 0
  }

  generateHideWord(){

    // Array.from permet de générer notre mot sous forme d'un tableau
    MOT = Array.from(MOTS[Math.floor(Math.random()*MOTS.length)])
    const mot = []

    for (let i = 0; i <= MOT.length-1; i++) {

      mot[i] = '_'
    }
    return mot
   }
  
  restartGame = onClick => {

    const { letter, letters, usedLetters, mot_a_deviner, tryCount, errorCount, won } = this.state
    this.setState({

      letter: '',
      letters: LETTERS,
      usedLetters: [],
      mot_a_deviner: this.generateHideWord(),
      tryCount: 0,
      errorCount: 0,
      score: 0,
      won: false,
      warn: ''
    })

    const canvas = document.getElementById('mon_canvas')
    const context = canvas.getContext('2d')
    context.clearRect(0,0,300,300)

    return

  }

  checkLetterClick = index => {
    
    const { letter, letters, usedLetters, mot_a_deviner, tryCount, errorCount, warn, score } = this.state
    const newLetter = letters[index]
    const buttonLetter = document.getElementsByClassName('letter')
    buttonLetter[index].classList.add('disabled')
    buttonLetter[index].getElementsByClassName(letter).disabled = true;
    const newMot = mot_a_deviner
    const newScore = score
    const newTryCount = tryCount
    const newErrorCount = errorCount
    this.setState({ warn : '' })
    this.setState({ letter: newLetter })
    let isIn = false
    const canvas = document.getElementById('mon_canvas')
    const context = canvas.getContext('2d')


    // Si la lettre a déjà été utilisé
    if(usedLetters.includes(newLetter)){

        this.setState({ warn: 'Lettre déjà utilisée' })

        // On suppose que le score minimal est 0 point
        if (newScore > 2) {
          this.setState({ score: newScore - 2})
        }
        // Il pourrait garder 1 point sinon
        else if(newScore === 1){
          this.setState({ score: newScore - 1})
        }
        return
    }

    // Sinon on incrément le nombre de try
    this.setState({ tryCount: newTryCount+1 })
    this.setState({ usedLetters: [...usedLetters, ...newLetter] })

    // Parcours notre Mot et voir si la lettre est dedans
    for (let i = 0; i <= MOT.length-1; i++) {
     
      if(newLetter === MOT[i]) {
        newMot[i] = newLetter   
        this.setState({ mot_a_deviner: newMot })
        isIn = true
      }    
    } 

    // Si la lettre est dans le mot
    if(isIn){
      this.setState({ score: newScore + 2 })
    }

    if(newMot.join() === MOT.join())
    {  
        this.setState({ won: true })
        return
    }

    // Sinon on commence à dessiner notre pendu
    if(!isIn) {

      this.setState({ usedLetters: [...usedLetters, ...newLetter] })
      this.setState({ errorCount: newErrorCount + 1 })
      const current_errors = newErrorCount + 1

      if (newScore > 0) {
        this.setState({ score: newScore - 1 })
      }
      

      // code trouvé sur codepen et arrangé à notre sauce
      // manière peut être non élégante peut etre utilisé un switch
      if(current_errors === 1) {
          context.beginPath() // On démarre un nouveau tracé
          context.lineCap = 'round'
          context.lineWidth = "10"
          context.lineJoin = 'round'
          context.strokeStyle = "rgb(23, 145, 167)"
          context.moveTo(35, 295) 
          context.lineTo(5, 295)
          context.stroke()
          return
      }
      else if(current_errors === 2) {
          context.moveTo(20, 295)
          context.lineTo(20, 5)
          context.stroke()
          return
      }         
      else if(current_errors === 3) {
          context.lineTo(200, 5)
          context.stroke()
          return
      }
      else if(current_errors === 4) {
          context.lineTo(200, 50)
          context.stroke()
          return
      }
      else if(current_errors === 5) { 
          context.moveTo(20, 50)
          context.lineTo(70, 5)
          context.stroke()
          return
      }
      else if(current_errors === 6) {
                 context.beginPath()
          context.fillStyle = "red"
          context.arc(200, 50, 20, 0, Math.PI * 2)
          context.fill()
          return
      }
      else if(current_errors === 7) {
          context.beginPath()
          context.strokeStyle = "red"
          context.moveTo(200, 50)
          context.lineTo(200, 150)
          context.stroke()
          return
      }
      else if(current_errors === 8) {
          context.beginPath()
          context.moveTo(200, 80)
          context.lineTo(160, 110)
          context.stroke()
          return
      }
      else if(current_errors === 9) {
          context.beginPath()
          context.moveTo(200, 80)
          context.lineTo(240, 110)
          context.stroke()
          return
      }
      else if(current_errors === 10) {
          context.beginPath()
          context.moveTo(200, 150)
          context.lineTo(180, 200)
          context.stroke()
          return
      }
      else if(current_errors === 11) {
          context.beginPath()
          context.moveTo(200, 150)
          context.lineTo(220, 200)
          context.stroke()
          context.beginPath()
          context.fillStyle = "rgb(23, 145, 167)"
          context.arc(200, 62, 16, 0, Math.PI * 2)
          context.fill()
          context.beginPath()
          context.fillStyle = "red"
          context.arc(200, 50, 20, 0, Math.PI * 2)
          context.fill()
          //return
      } 
    }
  }

  render() {

    const { letters, mot_a_deviner, tryCount, errorCount, won, score, warn } = this.state
    return (
      <div className="pendu">
        <div className="mot_deviner">
          <p>{mot_a_deviner}</p>
        </div>
        <div className="letters">
          {won || errorCount === 11 ? 
            <button onClick={this.restartGame}>Recommencez</button> :
            letters.map((letter, index) => 
              <Letter
                letter={letter}
                index={index}
                key={index}
                onClick={this.checkLetterClick}
                />
              )
          }
        </div>
        {warn !== '' && <p>{warn}</p>}
        {won ?
          <div className="result">
           <p>Vous avez gagné en {tryCount} essais !</p>
           <p>Votre score est de {score} points.</p>
          </div> : 
          <p>Nombre d'erreurs: {errorCount}</p>
        }
        <div className="canvas">
          <canvas id="mon_canvas" width="300" height="300"></canvas>
        </div>  
      </div>
    )  
  }
}

export default App
